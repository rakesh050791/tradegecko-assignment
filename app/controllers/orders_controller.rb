class OrdersController < ApplicationController

  include FileImporter
  include SharedMethods

  before_action :set_order, :set_time, only: [:order_status]

  def import
    unless params[:file].present?
      render_error_response(message: I18n.t('message.file_not_present'))
      return
    end

    begin
      upload(params[:file])
      render json: {message: t('message.file_upload_success')}, status: :ok
    rescue
      render_error_response(message: t('message.invalid_format')) && return
    end
  end

  def order_status
    status = @order.revision_at(@time)
    unless status
      render_error_response(message: t('message.order_not_found'), status: :unprocessable_entity)
    else
      render json: status, status: :ok
    end
  end

  private

  def set_order
    @order = Order.find_by(id: params['order_id'])
    render_error_response(message: t('message.order_not_found'), status: :not_found) unless @order
  end

  def set_time
    @time = unix_to_utc(params['order_time']) if params['order_time']
    render_error_response(message: t('message.invalid_input'), status: :unprocessable_entity) unless @time
  end

end