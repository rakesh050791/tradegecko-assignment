module Response

  def render_error_response(message:nil, errors:[], status: :unprocessable_entity)
    response = {
        "errors":
        {
        "message": message,
        **(errors.present? ? {error: errors} : {})
    }
    }
    render json: response, status: status
  end
end