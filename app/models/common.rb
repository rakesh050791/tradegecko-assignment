module Common
  extend ActiveSupport::Concern

  included do
    after_save :update_audit
  end

  def self.included(base)
    def base.fetch_valid_attributes(attributes_list)
      attributes_list.reject {|_key| !self.new.attribute_names.include?(_key.to_s)}.compact
    end
  end

  def update_data(data)
    self.update_attributes(self.class.fetch_valid_attributes(data))
  end

  def update_audit
    audit = self.audits.last
    audit.created_at = self.updated_at
    audit.save
  end
end
