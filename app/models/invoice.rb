class Invoice < ApplicationRecord
  include Common
  audited

  belongs_to :order, optional: true

  def update_invoice(data)
    self.order ||= Order.where(id: data['order_id']).first_or_create
    self.order.update_data(Order.fetch_valid_attributes(data))
    update_data(data)
  end

end
