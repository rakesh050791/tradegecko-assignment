class Customer < ApplicationRecord
  include Common
  audited

  has_many :orders
  has_many :products, through: :orders

  validates :customer_name, presence: true
  validates :customer_name, uniqueness: true

  def update_customer(data)
    update_data(data)
  end
end
