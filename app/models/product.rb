class Product < ApplicationRecord
  include Common
  audited

  has_and_belongs_to_many :orders

  validates :name, presence: true
  validates :name, uniqueness: true

  def attribute_names
    super + ['order_ids']
  end

  def update_product(data)
    update_data(data)
  end

end
