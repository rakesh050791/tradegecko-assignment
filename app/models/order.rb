class Order < ApplicationRecord
  include Common
  audited

  belongs_to :customer, optional: true
  has_one :invoice
  has_and_belongs_to_many :products

  def attribute_names
    super + ['product_ids']
  end

  def self.fetch_valid_attributes(attributes_list)
    attributes = attributes_list.reject {|_key| !self.new.attribute_names.include?(_key.to_s)}.compact
    attributes['product_ids'] = Product.where(id: attributes['product_ids']).pluck(:id) if attributes['product_ids'].present?
    attributes
  end

  def update_order(data)
    self.customer ||= Customer.where(id: data['customer_id']).first_or_create
    self.customer.update_data(Customer.fetch_valid_attributes(data))
    update_data(data)
  end
end
