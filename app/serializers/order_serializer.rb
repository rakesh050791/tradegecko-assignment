class OrderSerializer < ActiveModel::Serializer
  attributes :status, :customer_name, :customer_address, :ship_date, :shipping_provider

  # has_one :customer

  def customer_name
    object.customer.customer_name if object.customer
  end

  def customer_address
    object.customer.customer_address if object.customer
  end

  def ship_date
    object.ship_date if object.ship_date
  end

  def shipping_provider
    object.shipping_provider if object.shipping_provider
  end
end

