module SharedMethods

  def parse_obj_info row
    details = row['object_changes'].gsub('\\', '')
    object_changes = JSON.parse(details)
    object_changes
  end

  def unix_to_utc time
    DateTime.strptime(time, '%s')
  end

  def file_headers
    %w(object_id object_type timestamp object_changes)
  end

end