module FileImporter
  require 'csv'

  include SharedMethods

  module CsvFormats
    CSV = '.csv'
  end

  def file_errors
    return ['File not found'] unless @file.present?
    return ['Only csv file is supported'] if invalid_file_format?
    []
  end

  def invalid_file_format?
    [CsvFormats::CSV].exclude? File.extname(@file.path)
  end

  def file_valid(file_name)
    @file = File.new(file_name.path)
    file_errors
  end

  def upload file
    return file_valid(file) if file_valid(file).present?
    converter = lambda {|header| header.downcase.gsub(/[^\d,\_,a-z]/, '')}
    CSV.foreach(@file.path, headers: true, header_converters: converter, encoding: 'UTF-8') do |row|
      begin
        changes = parse_obj_info(row)
        object_class = row['object_type'].classify.constantize
        object = object_class.where(id: row['object_id']).first_or_create
        t = Time.at(row['timestamp'].to_i)
        object.update_attribute(:updated_at, t) #if t < object.created_at
        object.send("update_#{row['object_type'].downcase}", changes)
      rescue => e
        puts "Import fail for data (#{row}) \nError: #{e.inspect}"
      end
    end
  end
end