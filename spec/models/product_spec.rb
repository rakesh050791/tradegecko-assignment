require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'associations' do
    it {is_expected.to have_and_belong_to_many(:orders)}
  end

  context "validations" do
    it {is_expected.to validate_presence_of :name}
    it {is_expected.to validate_uniqueness_of :name}
  end

  it "has a valid factory" do
    expect(build(:product)).to be_valid
  end

  describe "public instance methods" do
    let(:product) {create(:product)}
    context "responds to its methods" do
      it {expect(product).to respond_to(:update_data)}
      it {expect(product).to respond_to(:update_product)}
      it {expect(product).to respond_to(:update_audit)}
    end

    context "executes methods correctly" do
      context "update_data" do
        it "should update data" do
          #TODO
        end
      end

      context "update_product" do
        it "should update product details" do
          #TODO
        end
      end

      context "update_audit" do
        it "should update created at for product audit" do
          #TODO
        end
      end
    end
  end
end