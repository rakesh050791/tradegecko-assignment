require 'rails_helper'

RSpec.describe Order, type: :model do
  context 'associations' do
    it {is_expected.to belong_to(:customer)}
    it {is_expected.to have_one(:invoice)}
    it {is_expected.to have_and_belong_to_many(:products)}
  end

  # context "validations" do
  #   it {is_expected.to validate_presence_of :id}
  #   it {is_expected.to validate_uniqueness_of :id}
  # end

  it "has a valid factory" do
    expect(build(:order)).to be_valid
  end

  describe "public instance methods" do
    let(:order) {create(:order)}
    context "responds to its methods" do
      it {expect(order).to respond_to(:update_data)}
      it {expect(order).to respond_to(:update_order)}
      it {expect(order).to respond_to(:update_audit)}
    end

    context "executes methods correctly" do
      context "update_data" do
        it "should update data" do
          #TODO
        end
      end

      context "update_order" do
        it "should update order details" do
          #TODO
        end
      end

      context "update_audit" do
        it "should update created at for order audit" do
          #TODO
        end
      end
    end
  end
end