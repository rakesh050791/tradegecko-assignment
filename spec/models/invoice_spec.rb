require 'rails_helper'

RSpec.describe Invoice, type: :model do
  context 'associations' do
    it {is_expected.to belong_to(:order)}
  end

  it "has a valid factory" do
    expect(build(:invoice)).to be_valid
  end

  describe "public instance methods" do
    let(:invoice) {create(:invoice)}
    context "responds to its methods" do
      it {expect(invoice).to respond_to(:update_data)}
      it {expect(invoice).to respond_to(:update_invoice)}
      it {expect(invoice).to respond_to(:update_audit)}
    end

    context "executes methods correctly" do
      context "update_data" do
        it "should update data" do
          #TODO
        end
      end

      context "update_invoice" do
        it "should update invoice details" do
          #TODO
        end
      end

      context "update_audit" do
        it "should update created at for invoice audit" do
          #TODO
        end
      end
    end
  end
end