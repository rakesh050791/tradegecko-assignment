require 'rails_helper'

RSpec.describe Customer, type: :model do
  context 'associations' do
    it {is_expected.to have_many(:orders)}
  end

  context "validations" do
    it {is_expected.to validate_presence_of :customer_name}
    it {is_expected.to validate_uniqueness_of :customer_name}
  end

  it "has a valid factory" do
    expect(build(:customer)).to be_valid
  end

  describe "public instance methods" do
    let(:customer) {create(:customer)}
    context "responds to its methods" do
      it {expect(customer).to respond_to(:update_data)}
      it {expect(customer).to respond_to(:update_customer)}
      it {expect(customer).to respond_to(:update_audit)}
    end

    context "executes methods correctly" do
      context "update_data" do
        it "should update data" do
          #TODO
        end
      end

      context "update_customer" do
        it "should update customer details" do
          #TODO
        end
      end

      context "update_audit" do
        it "should update created at for customer audit" do
          #TODO
        end
      end
    end
  end
end