require 'rails_helper'
include SharedMethods

describe OrdersController do

  before(:all) do
    Customer.destroy_all
    Invoice.destroy_all
    Product.destroy_all
    Order.destroy_all
  end

  let(:file) {Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/support/sample.csv')))}
  let(:invalid_file) {'sample.xlxs'}
  let(:order) {FactoryBot.attributes_for(:order)}
  let(:order) {create(:order)}

  context '#routes' do
    it 'post: orders/import should route to orders#import' do
      expect(post: '/orders/import').to route_to(
                                            controller: 'orders',
                                            action: 'import'
                                        )
    end

    it 'post: orders/order_status should route to orders#order_status' do
      expect(get: '/orders/order_status').to route_to(
                                                 controller: 'orders',
                                                 action: 'order_status'
                                             )
    end
  end

  context '#import', type: :request do
    it 'should raise error when file missing' do

      expected_res = {'errors' => {'message' => I18n.t('message.file_not_present')}}
      post '/orders/import', params: {file: ''}

      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)).to eq(expected_res)
    end

    it 'should raise error when invalid file format' do

      expected_res = {'errors' => {'message' => I18n.t('message.invalid_format')}}
      post '/orders/import', params: {file: invalid_file}

      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)).to eq(expected_res)
    end

    it 'should import data from file when valid' do
      expected_res = {'message' => I18n.t('message.file_upload_success')}
      post '/orders/import', params: {file: file}, headers: headers
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq(expected_res)
    end
  end

  context '#order_status', type: :request do
    it 'should render invalid input response' do
      expected_res = {'errors' => {'message' => I18n.t('message.invalid_input')}}
      get "/orders/order_status?order_id=#{order.id}"
      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)).to eq(expected_res)
    end

    it 'should render not_found response' do
      expected_res = {'errors' => {'message' => I18n.t('message.order_not_found')}}
      get "/orders/order_status?order_id=#{0}"
      expect(response).to have_http_status(:not_found)
      expect(JSON.parse(response.body)).to eq(expected_res)
    end


    #TODO need to mock audit for order.

    # before do
    #   response = double(body: "{status: #{order.status},customer_address: #{order.customer.customer_name},ship_date: '2017-01-18',shipping_provider: #{order.shipping_provider}}", code: 200)
    #   OrdersController.any_instance.stub(:order_status).and_return(order)
    # end
    #
    # it 'should render success response' do
    #   expected_res = {"status" => order.status,
    #                   "customer_name" => order.customer.customer_name,
    #                   "customer_address" => order.customer.customer_address,
    #                   "ship_date" => "2017-01-18",
    #                   "shipping_provider" => order.shipping_provider}
    #
    #   get "/orders/order_status?order_id=#{order.id}&order_time=1484730554"
    #   expect(response).to have_http_status(:ok)
    #   expect(JSON.parse(response.body)).to eq(expected_res)
    # end
  end
end