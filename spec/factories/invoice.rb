FactoryBot.define do
  factory :invoice, class: Invoice do |f|
    f.total {Faker::Commerce.price}
    order
  end
end