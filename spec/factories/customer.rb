FactoryBot.define do
  factory :customer, class: Customer do |f|
    f.customer_name {Faker::Name.name}
    f.customer_address {Faker::Address.street_address}
  end
end