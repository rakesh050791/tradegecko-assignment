FactoryBot.define do
  factory :order, class: Order do |f|
    f.status {"unoaid"}
    f.ship_date {"2017-01-18"}
    f.shipping_provider {"DHL"}
    customer
  end
end