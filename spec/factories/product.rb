FactoryBot.define do
  factory :product, class: Product do |f|
    f.name {Faker::Name.name}
    f.stock_levels {Faker::Number.number(4)}
    f.price {Faker::Commerce.price}
  end
end