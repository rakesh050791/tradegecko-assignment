# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

*Prerequisites
Assuming you have installed `git`, `ruby` and `rvm`.

* Ruby version
2.4.1

* Rails version
5.1.6

* To setup this application on local
1 - git clone git@bitbucket.org:rakesh050791/tradegecko-assignment.git 

2 - go to project directory :  cd trade_gecko and run : bundle 

3 - rake db:create, db:migrate

4 - run server : rails s 

5 - Go to browser and type : localhost:3000 

* How to run the test suite
bundle exec rspec 


* Basic flow
-- I have introduced two api end points, one for importing csv another one for getting object status.

-- user will input csv with import csv end point, will process the csv and will populate data in respective tables if valid csv is uploaded 

-- import csv end point will give error message if file format is not valid.

-- User can check the object status with api end point provided named order status: it will give order object if order exists .

-- If order doesn't exist it will give "Object Didn't Exist at that time" message.

-- The object status with api end point will be required to give two input parameters i.e. object_id & object_time

-- Postman collection link : https://www.getpostman.com/collections/4df1a17a2e2225dee4ef 


*Scope of improvement 

-- User authentication can also be incorporated.

-- For processing of large csv file we can put import csv task in the background job's (sidekiq, resque etc.)

-- more code optimisation can be done (as there will always be scope of refactoring :) )

-- exception notifier can be incorporated. 

-- test case  coverage can be imporoved (current coverage is 90% approx.)

-- Some sort of admin panel can also be incorporated so that user can check the uploaded data. 

* ...
