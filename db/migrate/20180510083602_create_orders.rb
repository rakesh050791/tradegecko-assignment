class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.string :status
      t.date :ship_date
      t.string :shipping_provider

      t.timestamps
    end
  end
end
