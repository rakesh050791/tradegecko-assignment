class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.references :order, foreign_key: true
      t.decimal :total, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
